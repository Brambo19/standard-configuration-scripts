#!/bin/bash

REPOPWD=`pwd`

# Moving to home dir
echo "Moving to $HOME"
cd $HOME

echo "Install vimrc plugin if not exists"
if [ -d "$HOME/.vim_runtime/" ]; then
    echo ".vim_runtime exists, skipping..."
else
    echo "cloning from awesome_vimrc..."
    git clone --depth=1 https://github.com/amix/vimrc.git ~/.vim_runtime
    sh ~/.vim_runtime/install_awesome_vimrc.sh

    # Move the my_configs.vim to .vim_runtime
    cd $REPOPWD
    cp my_configs.vim $HOME/.vim_runtime/
    cd $HOME
fi

echo "Install oh-my-zsh with plugins..."
if [ -d "$HOME/.oh_my_zsh/" ]; then
    echo "oh-my-zsh was installed"
else
    if [ ! `which zsh` ]; then
        echo "zsh was not installed"
    else
        echo "cloning from oh-my-zsh..."
        # Clone...
        sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
        echo "cloning plugins..."
        # clone zsh-autosuggestions
        git clone https://github.com/zsh-users/zsh-autosuggestions .oh_my_zsh/custom/plugins/zsh-autosuggestions
        # clone zsh-syntax-highlighting
        git clone https://github.com/zsh-users/zsh-syntax-highlighting.git .oh_my_zsh/custom/plugins/zsh-syntax-highlighting

        # Add to .zshrc plugins (Don't know how to do this automatically
        echo "To enable, add both plugins to .zshrc plugins"
    fi
fi


read -p "Do you wish to install nodejs? [y/n]" -n 1 -r
echo

if [[ $REPLY =~ ^[yYjJ]$ && ! `which node` ]]; then
    [ ! -d "$HOME/build" ] && mkdir -p $HOME/build
    echo "Moving to $HOME/build"
    cd $HOME/build
    echo "downloading node to build..."
    wget https://nodejs.org/dist/v12.16.3/node-v12.16.3-linux-x64.tar.xz -O nodejs.tar.xz
    echo "extracting node..."
    tar -xvf nodejs.tar.xz
    # add to path TODO
    # TMP
    echo "Add $HOME/build/nodejs/bin to PATH"

    echo "Moving back to $HOME"
    cd $HOME
fi

read -p "Do you wish to install mitmproxy? [y/n]" -n 1 -r
echo

if [[ $REPLY =~ ^[yYjJ]$ && ! `which mitmproxy` ]]; then
    if [ ! `which pip3` ]; then
        echo "pip3 not installed or not in path"
    else
        echo "Installing mitmproxy..."
        pip3 install mitmproxy
    fi
fi

cp $REPOPWD/.tmux.conf $HOME

echo "Done"
