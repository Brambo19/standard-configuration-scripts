autocmd FileType javascript set shiftwidth=2 softtabstop=2 expandtab
autocmd FileType vue set shiftwidth=2 softtabstop=2 expandtab
autocmd FileType typescript set shiftwidth=2 softtabstop=2 expandtab
autocmd FileType html set shiftwidth=2 softtabstop=2 expandtab
